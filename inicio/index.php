<?php

include ($_SERVER['DOCUMENT_ROOT']).'/php/header.php';
include ($_SERVER['DOCUMENT_ROOT']).'/inicio/header.php';

?>

<div class="container-fluid padd">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
	  <!-- Indicators -->
	 	<ol class="carousel-indicators">
	    	<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
	    	<li data-target="#myCarousel" data-slide-to="1"></li>
	    	<li data-target="#myCarousel" data-slide-to="2"></li>
	 	</ol>

	  <!-- Wrapper for slides -->
	  	<div class="carousel-inner">
	    	<div class="item active">
	      		<img src="/img/icon-black.jpg" alt="Mundini es una marca 100%">
	    	</div>
		    <div class="item">
		    	<img src="/img/producto/taller-mundini/kuni.jpg" alt="">
		    </div>

		    <div class="item">
		    	<img src="/img/producto/en-eventos/mordederas.jpg" alt="">
		    </div>
	  	</div>

	  <!-- Left and right controls -->
	  	<a class="left carousel-control" href="#myCarousel" data-slide="prev">
	    	<span class="glyphicon glyphicon-chevron-left"></span>
	    	<span class="sr-only">Previous</span>
	  	</a>
	  	<a class="right carousel-control" href="#myCarousel" data-slide="next">
	    	<span class="glyphicon glyphicon-chevron-right"></span>
	    	<span class="sr-only">Next</span>
	  	</a>
	</div>
</div>

<?php  
	include ($_SERVER['DOCUMENT_ROOT']).'/php/footer.php';
?>