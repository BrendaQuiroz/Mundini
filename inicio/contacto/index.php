<?php

include ($_SERVER['DOCUMENT_ROOT']).'/php/header.php';
include ($_SERVER['DOCUMENT_ROOT']).'/inicio/header.php';

require_once  ($_SERVER['DOCUMENT_ROOT']).'/php/contacto.php';

$contact=new Contacto;
if (!empty($_POST)) {
	$nombre=$_POST["nombre"];
	$apellidos=$_POST["apellidos"];
	$correo=$_POST["correo"];
	$mensaje=$_POST["mensaje"];
	$receivedMail=$contact->mailing($nombre,$apellidos,$correo,$mensaje);
}

?>
<div class="container-fluid padd contact-contain">
	<div class=" col-md-7 col-xs-12 " style="padding: 5%;">
		<div id="parrafo" class="title-catalogo">Contacta con nosotros</div>
		<p class="text-c">En mundini de Monini nos interesamos por su opini&oacute;n.</p>
		<p class="text-c">&hearts; Dudas.</p>
		<p class="text-c">&hearts; Sugerencias.</p>
		<p class="text-c">&hearts; Solicita platicas.</p>
		<p class="text-c">&hearts; Haz un pedido especial y &uacute;nico.</p>
	</div>	
	<div class="col-md-5 col-xs-12 mail">
		<div class="title-catalogo">Mandanos un correo:</div>
		<form method="POST" action="">
			<div class="col-xs-12 col-md-12">
				<label>Nombre</label>
				<input id="nombre" type="text" name="nombre" placeholder="Nombre">
			</div>
			<div class="col-xs-12 col-md-12">
				<label>Apellidos</label>
				<input id="apellidos" type="text" name="apellidos" placeholder="Apellidos">
			</div>
			<div class="col-xs-12 col-md-12">
				<label>Correo Electrn&oacute;nico</label>
				<input id="correo" type="email" name="correo" placeholder="Correo Elent&oacute;nico">
			</div>
			<div class="col-xs-12 col-md-12">
				<label>Mensaje</label>
				<input class="message" id="mensaje" type="text" name="mensaje" placeholder="M&aacute;ximo 1500 carat&eacute;res ">
			</div>
			<div class="col-xs-12 col-md-12 text-center">
				<input class="send-btn" type="submit" name="Enviar" value="Enviar">
			</div>
		</form>
	</div>
</div>

<?php  
	include ($_SERVER['DOCUMENT_ROOT']).'/php/footer.php';
?>