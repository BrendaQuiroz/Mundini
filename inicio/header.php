<?php 
  //header de barra de navigacion para seccion de publico general
 ?>

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div  class="navbar-header nav-title-width">
      <a class="navbar-brand icono-top" href="/inicio"><img class="icono" src="/img/mundini-icon.png"></a>
      <h3 class="nav-title">Mundini de Monini</h3>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right nav-right">
        <li><a href="/inicio">Inicio</a></li>
        <li><a href="/inicio/somos/">¿Qui&eacute;nes somos?</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            Productos<span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li><a href="/inicio/productos/catalogo/">Cat&aacute;logo de productos</a></li>
            <li><a href="/inicio/productos/elaboracion/">Forma de elaboraci&oacute;n</a></li>
            <li><a href="/inicio/productos/comprar/">¿C&oacute;mo comprar?</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            Actividades<span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li><a href="/inicio/actividades/lugares/">Lugares donde hemos estado</a></li>
            <li><a href="/inicio/actividades/solicitaPlaticas/">Solicita platicas</a></li>
            <li><a href="/inicio/actividades/platicas/">Platicas anteriores</a></li>
          </ul>
        </li>
        <li><a href="/inicio/ayudar/">C&oacute;mo ayudar</a></li>
        <li><a href="/inicio/contacto">Contacto</a></li>
        <li class="viewAs"><a href="/kids">Ver modo kids</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>