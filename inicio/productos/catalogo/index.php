<?php

include ($_SERVER['DOCUMENT_ROOT']).'/php/header.php';
include ($_SERVER['DOCUMENT_ROOT']).'/inicio/header.php';
require_once  ($_SERVER['DOCUMENT_ROOT']).'/php/mercancia.php';

$monito=new Mercancia;
$productPicture=array();
$productPicture=$monito->imagenesProductos();
?>

<div class="container-fluid padd">
	<div class="col-xs-12 col-md-12 text-left">
		<div class="title-catalogo">Diseñamos, Elaboramos y Distribuimos</div>
	</div>
	<div class="product-background">
		<div class="row catalog">
		    <?php
	        foreach ($productPicture as $idProduct => $productPic)
	        {
	        ?>
		        <div class="col-xs-6 col-sm-2 outline">
		            <a class="" >
		            	<p class="footi"><?= $productPic["name"]?> </p>
		            	<img src="<?= $productPic["url"]?>" alt="" class="imgCat img-responsive pointer">
		            </a>
		        </div>
	        <?php
	        }
		    ?>
		</div>
	</div>
</div>

<?php  
	include ($_SERVER['DOCUMENT_ROOT']).'/php/footer.php';
?>