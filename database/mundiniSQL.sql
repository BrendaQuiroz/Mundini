-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-05-2018 a las 08:37:17
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mundini`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `c_mundini_autor`
--

CREATE TABLE `c_mundini_autor` (
  `c_mundini_autor_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_autor_nombre` varchar(20) NOT NULL,
  `c_mundini_autor_apellidos` varchar(45) DEFAULT NULL,
  `c_mundini_autor_nacionalidad_id` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `c_mundini_coloreable`
--

CREATE TABLE `c_mundini_coloreable` (
  `c_mundini_coloreable_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_coloreable_titulo` text NOT NULL,
  `c_mundini_coloreable_autor_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_coloreable_tipo_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_coloreable_anio` year(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `c_mundini_comics`
--

CREATE TABLE `c_mundini_comics` (
  `c_mundini_comics_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_comics_titulo` text NOT NULL,
  `c_mundini_comics_autor_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_comics_tipo_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_comics_genero_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_comics_anio` year(4) DEFAULT NULL,
  `c_mundini_comics_paginas` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `c_mundini_cuento`
--

CREATE TABLE `c_mundini_cuento` (
  `c_mundini_cuento_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_cuento_titulo` text NOT NULL,
  `c_mundini_cuento_autor_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_cuento_genero_id` tinyint(3) UNSIGNED DEFAULT NULL,
  `c_mundini_cuento_tipo_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_cuento_anio` year(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `c_mundini_descargables`
--

CREATE TABLE `c_mundini_descargables` (
  `c_mundini_descargables_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_descargables_tipo` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `c_mundini_descargables`
--

INSERT INTO `c_mundini_descargables` (`c_mundini_descargables_id`, `c_mundini_descargables_tipo`) VALUES
(1, 'Coloreable'),
(2, 'Comic'),
(3, 'Cuento');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `c_mundini_genero`
--

CREATE TABLE `c_mundini_genero` (
  `c_mundini_genero_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_genero_nombre` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `c_mundini_nacionalidad`
--

CREATE TABLE `c_mundini_nacionalidad` (
  `c_mundini_nacionalidad_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_nacionalidad_pais` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `c_mundini_nacionalidad`
--

INSERT INTO `c_mundini_nacionalidad` (`c_mundini_nacionalidad_id`, `c_mundini_nacionalidad_pais`) VALUES
(1, 'Mexicana'),
(2, 'Estadounidesnse'),
(3, 'Canadiense'),
(4, 'Francesa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `c_mundini_producto`
--

CREATE TABLE `c_mundini_producto` (
  `c_mundini_producto_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_producto_nombre` varchar(45) NOT NULL,
  `c_mundini_producto_precio` decimal(5,2) DEFAULT NULL,
  `c_mundini_producto_tipo_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_producto_url` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `c_mundini_producto`
--

INSERT INTO `c_mundini_producto` (`c_mundini_producto_id`, `c_mundini_producto_nombre`, `c_mundini_producto_precio`, `c_mundini_producto_tipo_id`, `c_mundini_producto_url`) VALUES
(1, 'Arena para gatos 3kg', '35.00', 1, '/img/producto/producto-finales/arena/3kg.jpg'),
(2, 'Arena para gatos 10kg', '90.00', 1, '/img/producto/producto-finales/arena/10kg.jpg'),
(3, 'Arena para gatos 25kg', '210.00', 1, '/img/producto/producto-finales/arena/25kg.jpg'),
(4, 'Baberos', '401.00', 2, '/img/producto/producto-finales/baberos/babero.jpg'),
(5, 'Batas', '370.00', 3, '/img/producto/producto-finales/batas/bata_perros.jpg'),
(6, 'Cactus Tejidos', '80.00', 4, '/img/producto/producto-finales/cactus-tejidos/cactus-g.jpg'),
(7, 'Corino Ch', '180.00', 5, '/img/producto/producto-finales/corinos/gato-oxfor-ch.jpg'),
(8, 'Corino G', '290.00', 5, '/img/producto/producto-finales/corinos/gato-amarillo-amarillo-G.jpg'),
(9, 'Yima Ch', '110.00', 6, '/img/producto/producto-finales/corinos-amigos/yima-bebe.jpg'),
(10, 'Yima G', '135.00', 6, '/img/producto/producto-finales/corinos-amigos/yima-G.jpg'),
(11, 'Chancho', '215.00', 6, '/img/producto/producto-finales/corinos-amigos/chanito.jpg'),
(12, 'Frida', '85.00', 6, NULL),
(13, 'Buho', '130.00', 6, NULL),
(14, 'Tipi', '350.00', 7, '/img/producto/producto-finales/gatipis/tipi.jpg'),
(15, 'Sin Cara Ch', '50.00', 8, '/img/producto/producto-finales/ghibli/sin-cara-llavero.jpg'),
(16, 'Sin Cara G', '100.00', 8, '/img/producto/producto-finales/ghibli/sin-cara-g.jpg'),
(17, 'Cabeza de Nabo Ch', '260.00', 8, '/img/producto/producto-finales/ghibli/nabo-ch.jpg'),
(18, 'Cabeza de Nabo G', '330.00', 8, '/img/producto/producto-finales/ghibli/nabo-g.jpg'),
(19, 'Totoro Llavero', '50.00', 8, '/img/producto/producto-finales/ghibli/totoro-llavero.jpg'),
(20, 'Totoro Normal', '130.00', 8, '/img/producto/producto-finales/ghibli/totoro.jpg'),
(21, 'Ponycornio', '190.00', 9, '/img/producto/producto-finales/ponycornios/ponycornio-tricolor.jpeg'),
(22, 'Linea Humarinos', '0.00', 10, '/img/producto/producto-finales/humarinos/primitos.jpg'),
(23, 'Payaso Eso', '0.00', 11, '/img/producto/producto-finales/tierno_terror/eso-3.jpg'),
(24, 'R2D2', '230.00', 12, '/img/producto/producto-finales/mundini_geek/star-wars/r2d2-2.jpeg'),
(25, 'Elefante', '230.00', 13, '/img/producto/producto-finales/pedidos especiales/elefante-2.jpg'),
(26, 'La Mosca', '240.00', 13, '/img/producto/producto-finales/pedidos especiales/mosca.jpeg'),
(27, 'Minion', '260.00', 13, '/img/producto/producto-finales/pedidos especiales/minion-2.jpg'),
(28, 'Chimuelo', '300.00', 13, '/img/producto/producto-finales/pedidos especiales/chimuelo-6.jpg'),
(29, 'Monillo Luneta', '60.00', 14, '/img/producto/producto-finales/monillo/loneta-rosa.jpg'),
(30, 'Monillo Suet', '165.00', 14, '/img/producto/producto-finales/monillo/gato-negro-g.jpg'),
(31, 'Sonaja Ch', '40.00', 15, '/img/producto/producto-finales/juguetes/sonajas-ch.jpg'),
(32, 'Sonaja G', '60.00', 15, '/img/producto/producto-finales/juguetes/sonajas-G.jpg'),
(33, 'Pulsera Ch', '10.00', 15, NULL),
(34, 'Pulsara G', '15.00', 15, NULL),
(35, 'Cascabel Espiral', '37.00', 15, '/img/producto/producto-finales/juguetes/espiral-caramelo.jpg'),
(36, 'Mordedera', '40.00', 15, '/img/producto/producto-finales/juguetes/nudos.jpg'),
(37, 'Pelota', '27.00', 15, '/img/producto/producto-finales/juguetes/pelotas.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `c_mundini_puesto`
--

CREATE TABLE `c_mundini_puesto` (
  `c_mundini_puesto_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_puesto_nombre` varchar(30) DEFAULT NULL,
  `c_mundini_puesto_alias` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `c_mundini_puesto`
--

INSERT INTO `c_mundini_puesto` (`c_mundini_puesto_id`, `c_mundini_puesto_nombre`, `c_mundini_puesto_alias`) VALUES
(1, 'Socio', 'Socio'),
(2, 'Desarrollador Web', 'Desarrollador'),
(3, 'Jefe de Ventas', 'Ventas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `c_munidni_tipo_producto`
--

CREATE TABLE `c_munidni_tipo_producto` (
  `c_mundini_tipo_producto_id` tinyint(3) UNSIGNED NOT NULL,
  `c_mundini_tipo_producto_nombre` varchar(45) NOT NULL,
  `c_mundini_tipo_producto_material` varchar(50) DEFAULT NULL,
  `c_mundini_tipo_producto_descripcion` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `c_munidni_tipo_producto`
--

INSERT INTO `c_munidni_tipo_producto` (`c_mundini_tipo_producto_id`, `c_mundini_tipo_producto_nombre`, `c_mundini_tipo_producto_material`, `c_mundini_tipo_producto_descripcion`) VALUES
(1, 'Arena para gatos', 'Arena', 'Arena para gatos organica de marca ----'),
(2, 'Baberos', 'Tela', 'Baberos hechos para bebes de edades entre ---'),
(3, 'Batas', 'Tela', 'Batas con impresos variados para niños pequeños de-- edad'),
(4, 'Cactus Tejidos', 'Estambre y relleno industrial', 'Tejidos de especies distintas de cactus, incluyen sus macetas'),
(5, 'Corinos', 'Estambre y relleno industrial', 'Pieza especial de mundini, gatitos de distintos colores y tamaños'),
(6, 'Corinos Amigos', 'Estambre y relleno industrial', 'Piezas hechas de estambre de personajes nuevos en mundini'),
(7, 'Gatipis', 'Madera, tela y listones de licra', 'Tipis hechos para que los gatos habiten en ellos y se entretengan'),
(8, 'Linea Ghibli', 'Estambre y relleno industrial', 'Tejidos inspirados en distintos personajes de estudios Ghibli'),
(9, 'Linea Ponycornio', 'Estambre y relleno industrial', 'Tejidos inspirados en unicornios y ponys'),
(10, 'Linea Humarinos', 'Estambre y relleno industrial', 'Tejidos de prsonajes con forma humana'),
(11, 'Linea Dulce Terror', 'Estambre y relleno industrial', 'Tejidos inspirados en personajes de historias de terror'),
(12, 'Linea Geek', 'Estambre y relleno industrial', 'Tejidos inspirados en distintos personajes de la ciencia ficcion y el anime'),
(13, 'Pedidos especiales', 'Estambre y relleno industrial', 'Tejidos que disntintos clientes han han pedido en forma de adorables muñequitos'),
(14, 'Monillo', 'Tela y Relleno industrial', 'Uno de los primeros personajes clasicos de Mundini'),
(15, 'Juguetes', 'Materiales varios', 'Linea de juguetes para bebes como para mascotas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `f_mundini_correos_recibidos`
--

CREATE TABLE `f_mundini_correos_recibidos` (
  `f_correos_recibidos_id` smallint(5) UNSIGNED NOT NULL,
  `f_correos_recibidos_nombre_remitente` varchar(30) NOT NULL,
  `f_correos_recibidos_apellidos_remitente` varchar(40) NOT NULL,
  `f_correos_recibidos_mail_remitente` varchar(50) NOT NULL,
  `f_correos_recibidos_descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `o_mundini_usuario`
--

CREATE TABLE `o_mundini_usuario` (
  `o_mundini_usuario_id` tinyint(3) UNSIGNED NOT NULL,
  `o_mundini_usuario_nombre` varchar(30) DEFAULT NULL,
  `o_mundini_usuario_apellido` varchar(40) DEFAULT NULL,
  `o_mundini_usuario_puesto_id` tinyint(3) UNSIGNED DEFAULT NULL,
  `o_mundini_usuario_correo` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `o_mundini_usuario`
--

INSERT INTO `o_mundini_usuario` (`o_mundini_usuario_id`, `o_mundini_usuario_nombre`, `o_mundini_usuario_apellido`, `o_mundini_usuario_puesto_id`, `o_mundini_usuario_correo`) VALUES
(1, 'Mariana', 'Garcilazo Angel', 1, 'mariana@mundinidemonini.com'),
(2, 'Cecilia', 'Garcilazo Angel', 1, 'cecilia@mundinidemonini.com');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `c_mundini_autor`
--
ALTER TABLE `c_mundini_autor`
  ADD PRIMARY KEY (`c_mundini_autor_id`),
  ADD KEY `FK_autor_pais` (`c_mundini_autor_nacionalidad_id`);

--
-- Indices de la tabla `c_mundini_coloreable`
--
ALTER TABLE `c_mundini_coloreable`
  ADD PRIMARY KEY (`c_mundini_coloreable_id`),
  ADD KEY `FK_color_1` (`c_mundini_coloreable_tipo_id`),
  ADD KEY `FK_coloreable_autor` (`c_mundini_coloreable_autor_id`);

--
-- Indices de la tabla `c_mundini_comics`
--
ALTER TABLE `c_mundini_comics`
  ADD PRIMARY KEY (`c_mundini_comics_id`),
  ADD KEY `FK_comics_1` (`c_mundini_comics_tipo_id`),
  ADD KEY `FK_comic_genero` (`c_mundini_comics_genero_id`),
  ADD KEY `FK_comics_autor` (`c_mundini_comics_autor_id`);

--
-- Indices de la tabla `c_mundini_cuento`
--
ALTER TABLE `c_mundini_cuento`
  ADD PRIMARY KEY (`c_mundini_cuento_id`),
  ADD KEY `FK_cuento_1` (`c_mundini_cuento_tipo_id`),
  ADD KEY `FK_cuento_genero` (`c_mundini_cuento_genero_id`),
  ADD KEY `FK_cuento_autor` (`c_mundini_cuento_autor_id`);

--
-- Indices de la tabla `c_mundini_descargables`
--
ALTER TABLE `c_mundini_descargables`
  ADD PRIMARY KEY (`c_mundini_descargables_id`);

--
-- Indices de la tabla `c_mundini_genero`
--
ALTER TABLE `c_mundini_genero`
  ADD PRIMARY KEY (`c_mundini_genero_id`);

--
-- Indices de la tabla `c_mundini_nacionalidad`
--
ALTER TABLE `c_mundini_nacionalidad`
  ADD PRIMARY KEY (`c_mundini_nacionalidad_id`);

--
-- Indices de la tabla `c_mundini_producto`
--
ALTER TABLE `c_mundini_producto`
  ADD PRIMARY KEY (`c_mundini_producto_id`),
  ADD KEY `fk_tipo_idx` (`c_mundini_producto_tipo_id`);

--
-- Indices de la tabla `c_mundini_puesto`
--
ALTER TABLE `c_mundini_puesto`
  ADD PRIMARY KEY (`c_mundini_puesto_id`);

--
-- Indices de la tabla `c_munidni_tipo_producto`
--
ALTER TABLE `c_munidni_tipo_producto`
  ADD PRIMARY KEY (`c_mundini_tipo_producto_id`);

--
-- Indices de la tabla `f_mundini_correos_recibidos`
--
ALTER TABLE `f_mundini_correos_recibidos`
  ADD PRIMARY KEY (`f_correos_recibidos_id`);

--
-- Indices de la tabla `o_mundini_usuario`
--
ALTER TABLE `o_mundini_usuario`
  ADD PRIMARY KEY (`o_mundini_usuario_id`),
  ADD KEY `fk_puesto_idx` (`o_mundini_usuario_puesto_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `c_mundini_autor`
--
ALTER TABLE `c_mundini_autor`
  MODIFY `c_mundini_autor_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `c_mundini_coloreable`
--
ALTER TABLE `c_mundini_coloreable`
  MODIFY `c_mundini_coloreable_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `c_mundini_comics`
--
ALTER TABLE `c_mundini_comics`
  MODIFY `c_mundini_comics_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `c_mundini_cuento`
--
ALTER TABLE `c_mundini_cuento`
  MODIFY `c_mundini_cuento_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `c_mundini_descargables`
--
ALTER TABLE `c_mundini_descargables`
  MODIFY `c_mundini_descargables_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `c_mundini_genero`
--
ALTER TABLE `c_mundini_genero`
  MODIFY `c_mundini_genero_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `c_mundini_nacionalidad`
--
ALTER TABLE `c_mundini_nacionalidad`
  MODIFY `c_mundini_nacionalidad_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `c_mundini_producto`
--
ALTER TABLE `c_mundini_producto`
  MODIFY `c_mundini_producto_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `c_mundini_puesto`
--
ALTER TABLE `c_mundini_puesto`
  MODIFY `c_mundini_puesto_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `c_munidni_tipo_producto`
--
ALTER TABLE `c_munidni_tipo_producto`
  MODIFY `c_mundini_tipo_producto_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `f_mundini_correos_recibidos`
--
ALTER TABLE `f_mundini_correos_recibidos`
  MODIFY `f_correos_recibidos_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `o_mundini_usuario`
--
ALTER TABLE `o_mundini_usuario`
  MODIFY `o_mundini_usuario_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `c_mundini_autor`
--
ALTER TABLE `c_mundini_autor`
  ADD CONSTRAINT `FK_autor_pais` FOREIGN KEY (`c_mundini_autor_nacionalidad_id`) REFERENCES `c_mundini_nacionalidad` (`c_mundini_nacionalidad_id`);

--
-- Filtros para la tabla `c_mundini_coloreable`
--
ALTER TABLE `c_mundini_coloreable`
  ADD CONSTRAINT `FK_color_1` FOREIGN KEY (`c_mundini_coloreable_tipo_id`) REFERENCES `c_mundini_descargables` (`c_mundini_descargables_id`),
  ADD CONSTRAINT `FK_coloreable_autor` FOREIGN KEY (`c_mundini_coloreable_autor_id`) REFERENCES `c_mundini_autor` (`c_mundini_autor_id`);

--
-- Filtros para la tabla `c_mundini_comics`
--
ALTER TABLE `c_mundini_comics`
  ADD CONSTRAINT `FK_comic_genero` FOREIGN KEY (`c_mundini_comics_genero_id`) REFERENCES `c_mundini_genero` (`c_mundini_genero_id`),
  ADD CONSTRAINT `FK_comics_1` FOREIGN KEY (`c_mundini_comics_tipo_id`) REFERENCES `c_mundini_descargables` (`c_mundini_descargables_id`),
  ADD CONSTRAINT `FK_comics_autor` FOREIGN KEY (`c_mundini_comics_autor_id`) REFERENCES `c_mundini_autor` (`c_mundini_autor_id`);

--
-- Filtros para la tabla `c_mundini_cuento`
--
ALTER TABLE `c_mundini_cuento`
  ADD CONSTRAINT `FK_cuento_1` FOREIGN KEY (`c_mundini_cuento_tipo_id`) REFERENCES `c_mundini_descargables` (`c_mundini_descargables_id`),
  ADD CONSTRAINT `FK_cuento_autor` FOREIGN KEY (`c_mundini_cuento_autor_id`) REFERENCES `c_mundini_autor` (`c_mundini_autor_id`),
  ADD CONSTRAINT `FK_cuento_genero` FOREIGN KEY (`c_mundini_cuento_genero_id`) REFERENCES `c_mundini_genero` (`c_mundini_genero_id`);

--
-- Filtros para la tabla `c_mundini_producto`
--
ALTER TABLE `c_mundini_producto`
  ADD CONSTRAINT `fk_tipo` FOREIGN KEY (`c_mundini_producto_tipo_id`) REFERENCES `c_munidni_tipo_producto` (`c_mundini_tipo_producto_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `o_mundini_usuario`
--
ALTER TABLE `o_mundini_usuario`
  ADD CONSTRAINT `fk_puesto` FOREIGN KEY (`o_mundini_usuario_puesto_id`) REFERENCES `c_mundini_puesto` (`c_mundini_puesto_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
