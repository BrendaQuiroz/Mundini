<?php

class ConexionDB
{
	private $servername = "localhost";
	private $username = "root";
	private $password = "";
	private $database="mundini";
	private $conn = null;

	public function connect() {
        $conn=new mysqli($this->servername,$this->username,$this->password,$this->database);
        mysqli_set_charset( $conn, 'utf8');
        if($conn->connect_error)
        {
            die("Conexi&oacute;n fallida: ".$conn->connect_error);
            //return false;
        }
        return $conn;
    }
	
}
