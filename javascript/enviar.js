function enviar(){
	data = $("$form").serialize();
	var emailReject = new RegExp(/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i);

	if($("#nombre").val() === "" || !isNaN($("#nombre").val()))
    {
        $(".form-group").removeClass('has-error');
        $("#check_name").addClass('has-error');
        $("#nombre").focus();
        return false;
    }

    if($("#apellidos").val()==="" || !isNaN($("#apellidos").val())){
        $(".form-group").removeClass('has-error');
        $("#check_lastname").addClass('has-error');
        $("#apellidos").focus();
        return false;
    }

    if($("#correo").val()===""){
        $(".form-group").removeClass('has-error');
        $("#check_mail").addClass('has-error');
        $("#correo").focus();
        return false;
    }

     if(!emailRegex.test($("#correo").val()))
    {
        $next=false;
        $("#mail").closest(".form-group").addClass("has-error");
        $("#correo").focus();
        return false;
    }

    if($("#mensaje").val() === "" || !isNaN($("#mensaje").val()))
    {
        $(".form-group").removeClass('has-error');
        $("#check_name").addClass('has-error');
        $("#mensaje").focus();
        return false;
    }

    $.ajax({
        type: 'POST',
        url: '/php/Contacto/contactoController.php',
        data: data,
        success: function(resp) {
            if(location.pathname == "/login/")
            {
                location.replace("../perfil/?cuenta="+resp.Success);
            }
        }
    });
}