<?php
/**
 * Created by Juan Gonzalez.
 * Date: 09/01/2018
 * Time: 08:20 AM
 */

require_once ($_SERVER['DOCUMENT_ROOT']).'/database/conexionDB.php';
class Mercancia
{
	public function imagenesProductos(){
        $database=new ConexionDB;
    	$dbConnect=$database->connect();
    	$img = array();
        $sql="SELECT c_mundini_producto_id, c_mundini_producto_url, c_mundini_producto_nombre
                FROM c_mundini_producto
                WHERE c_mundini_producto_url is not NULL";
        $queryDB = $dbConnect -> query($sql);
        if($queryDB->num_rows>0) {
            while($row=$queryDB->fetch_assoc())
            {
                $img[$row["c_mundini_producto_id"]]=array("url" => $row["c_mundini_producto_url"], "name"=>$row["c_mundini_producto_nombre"]);
            }
            return $img;
        }
        $dbConnect->close();
    }
}