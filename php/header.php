<!DOCTYPE html>
<html >
<head>
    <title> Mundini de Monini</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="/img/mundini-logo.png"> 
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/css/carrusel.css">
    <link rel="stylesheet" href="/css/style.min.css" />
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/kids.css" />
    <link rel="stylesheet" href="/css/general.css"/> 
    <script src="/javascript/jquery-1.12.4.min.js"></script>
    <script src="/javascript/bootstrap.min.js"></script>
    <script src="/javascript/carrusel.js"></script>
    <script src="/javascript/fontawesome-all.js"></script>
    <script src="/javascript/jquery-ui.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Stalemate" rel="stylesheet">
</head>
<body>
    <div class=" main-container ">
