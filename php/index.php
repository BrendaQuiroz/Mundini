<?php
/**
 * Created by Juan Gonzalez.
 * Date: 09/01/2018
 * Time: 08:20 AM
 */

session_start();
if(isset($_SESSION["user"]))
    header("Location: ../perfil?cuenta=".$_SESSION["iduser"]);
include ($_SERVER['DOCUMENT_ROOT']).'/login/header.php';
require_once  ($_SERVER['DOCUMENT_ROOT']).'/php/catalogo_autos/version.php';



?>
<div class="container-fluid login-container" >
	<div class="column column1 "  >
        <img src="/img/logo_horizontal.png" alt="" class="login-logo">
		<div id="registrate" class="container-fluid login-font" >
			<strong><h2 class="login-title">Registrate</h2></strong> 
			<form onsubmit="return false;" class="form-size"> 
                <div class="col-md-6 col-sm-6">
                    <div id="check_name" class="form-group"> 
                        <label for="signUpName">Nombre</label>
                        <input type="text" class="form-control form-style" name="signUpName"  id="signUpName" placeholder="Nombre" onkeypress="registrarpress(event)">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div id="check_lastname" class="form-group">
                        <label for="signUpLastName">Apellidos</label>
                        <input type="text" class="form-control form-style" name="signUpLastName" id="signUpLastName" placeholder="Apellidos" onkeypress="registrarpress(event)">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div id="check_mail" class="form-group"> 
                        <label for="signUpEmail">Correo Electrónico</label>
                        <input type="email" class="form-control form-style" name="signUpEmail" id="signUpEmail" placeholder="E-mail" onchange="confirmmail($(this))" onkeypress="registrarpress(event)">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div id="check_username" class="form-group"> 
                        <label for="signUpUsername">Nombre de Usuario</label>
                        <input type="text" class="form-control form-style" name="signUpUsername" id="signUpUsername" placeholder="Nombre de Usuario" onkeypress="registrarpress(event)">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div id="check_pwd" class="form-group">
                        <label for="signUpPassword">Contraseña</label>
                        <input type="password" class="form-control form-style" name="signUpPassword" id="signUpPassword" placeholder="Mínimo 8 carácteres" onkeypress="registrarpress(event)">
                    </div>
                </div>  
                <div class="col-md-6 col-sm-6">
                    <div id="recheck_pwd" class="form-group">
                        <label for="reSignUpPassword">Confirma tu Contraseña</label>
                        <input type="password" class="form-control form-style" name="reSignUpPassword" id="reSignUpPassword" placeholder="Mínimo 8 carácteres" onkeypress="registrarpress(event)">
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="check_birth" class="form-group birth">
                        <label for="signUpdate1">Fecha de Nacimiento</label>
                        <?php
                        $year1 =date("Y-m-d",strtotime("- 16 years"));
                        $year2 =date("Y-m-d",strtotime("- 99 years"));
                        ?>
                        <input type="date" class="form-birth" name="signUpBirthdate" id="signUpBirthdate" step="1" min="1900-01-01" max= "<?= $year1?>" onkeypress="registrarpress(event)">
                    </div>
                </div>
        
                <div class="col-xs-12 text-center" style="width: 150px;">
                    <!--<p class="text-bold">Los robots a&uacute;n no pueden vender autos</p-->                    
                    <div class="g-recaptcha captcha" align="center" data-sitekey="6LezcEUUAAAAAPmmOzTQckUo9MQDMqVjpRXxvY6D"></div>
                </div> 
            </form> 
            <button id="regis" type="submit" class="btn btn-default login-btn" onclick="enviar()"> Registrarse </button>
		</div>

        <div id="logeate" class="container-fluid login-font hidden form-login" >
            <strong><h2 class="login-title">Inicia Sesión</h2></strong> 
            <form onsubmit="return false;" class="form-size form-session">
                <div id="in_username" class="form-group"> <!-- User Name -->
                    <label for="logInUsername">Nombre de Usuario o Correo Eletr&oacute;nico</label>
                    <input type="text" class="form-control form-style" name="logInUsername" id="logInUsername" onkeypress="iniciarpress(event)" placeholder="Nombre de Usuario">
                </div>
                <div id="in_pwd" class="form-group"> <!-- Password -->
                    <label for="logInPassword">Contraseña</label>
                    <input type="password" class="form-control form-style" name="logInPassword" id="logInPassword" onkeypress="iniciarpress(event)" placeholder="Mínimo 8 carácteres">
                </div>
            </form> 
            <button id="iniciar" type="submit" class="btn btn-default login-btn" onclick="conectar()"> Iniciar Sesi&oacute;n </button>
        </div>
	</div>
	<div class="column column2 col-xs-12" >
		<h2 class="text-m"> Continuar con: </h2>
        
        <div class="btn twitter-btn" onclick="loginTwitter()">
          <span class="icont"></span>
          <span class="buttonText btn-padd"> Twitter</span>
        </div>
        <div class="btn g-signin2" data-onsuccess="onSignIn"></div>
        <div id="name"></div>

        <div id="fbLink"  class="fb-login-button facebook-btn" data-max-rows="1" data-size="large" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false" login_text="Facebook" scope="public_profile,email" onlogin="checkLoginState();" href="javascript:void(0);"></div>
        <div>
            <h4 style="color: black;padding: 13px 0px 0px;">¿Ya tienes cuenta?</h4>
            <a class="login-now" href="#" data-target="#inicio"  data-toggle="modal">¡Inicia sesión!</a>
        </div>
	</div>
</div>


<div id="fb-root"></div>
<script>
    

    $(".login-now").click(function(){
       
        if($("#registrate").hasClass('hidden'))
        {
            $("#logeate").addClass('hidden');
            $("#registrate").removeClass('hidden');
            $(this).siblings("h4").text("¿Ya tienes cuenta?");
            $(this).text("¡Inicia sesión!");
        }
        else
        {
            $("#registrate").addClass('hidden');
            $("#logeate").removeClass('hidden');
            $(this).text("¡Registrate!");
            $(this).siblings("h4").text("¿No tienes cuenta?");
        }
    });

    window.fbAsyncInit = function() {
        FB.init({
          appId      : '1651717514914692',
          cookie     : true,
          xfbml      : true,
          version    : 'v2.12'
        });  
    FB.AppEvents.logPageView();   
    };
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.12&appId=1651717514914692&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    /*var googleUser = {};
    var startApp = function() {
    gapi.load('auth2', function(){
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      auth2 = gapi.auth2.init({
        client_id: 'YOUR_CLIENT_ID.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      attachSignin(document.getElementById('customBtn'));
    });
  };

  function attachSignin(element) {
    console.log(element.id);
    auth2.attachClickHandler(element, {},
        function(googleUser) {
          document.getElementById('name').innerText = "Signed in: " +
              googleUser.getBasicProfile().getName();
        }, function(error) {
          alert(JSON.stringify(error, undefined, 2));
        });
  }*/

</script>
<?php
    include ($_SERVER['DOCUMENT_ROOT']).'/login/footer.php';

?>