<?php 
  //header de barra de navigacion para seccion de niños
 ?>

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div  class="navbar-header nav-title-width">
      <a class="navbar-brand icono-top" href="/kids"><img class="icono" src="/img/mundini-icon.png"></a>
      <h3 class="nav-title">Mundini de Monini <span class="nav-subtitle">Kids</span></h3>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right nav-right">
        <li><a href="/kids">Inicio</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Quiero ver <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/kids/conciencia/">Concientizaci&oacute;n</a></li>
            <li><a href="/kids/creaciones/">Productos y Creaciones</a></li>
            <li><a href="/kids/descarga/">Descargables</a></li>
            <li><a href="/kids/juegos/">Juegos</a></li>
          </ul>
        </li>
        
        <li><a href="/kids/ayudar/">Quiero ayudar</a></li>
        <li><a href="/kids/contacto">Contacto</a></li>
        <li class="viewAs"><a href="/inicio">Ver modo general</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>