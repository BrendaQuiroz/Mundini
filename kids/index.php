<?php

include ($_SERVER['DOCUMENT_ROOT']).'/php/header.php';
include ($_SERVER['DOCUMENT_ROOT']).'/kids/header.php';

?>

<div class="container-fluid padd">
		<div class="row">
			<div class="col-md-3 division concience-section" >
				<div onclick="location.href='/kids/conciencia/'" class="overlay">
					<div class="title">Concientizaci&oacute;n</div> 
				</div>
			</div>
			<div class="col-md-3 division product-section">
				<div onclick="location.href='/kids/creaciones/'" class="overlay">
					<div class="title">Productos y Creaciones</div> 
				</div>
				
			</div>
			<div class="col-md-3 division download-section">
				<div onclick="location.href='/kids/descarga/'" class="overlay">
					<div class="title">Descargables</div> 
				</div>
				
			</div>
			<div onclick="location.href='/kids/juegos/'" class="col-md-3 division game-section">
				<div class="overlay">
					<div class="title">Juegos</div> 
				</div>
			</div>
			
		</div>
	</div>

<?php  
	include ($_SERVER['DOCUMENT_ROOT']).'/php/footer.php';
?>